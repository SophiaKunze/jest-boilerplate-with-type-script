// stonk -> “Invalid” 
// pass word -> “Invalid” 
// password -> “Weak” 
// 11081992 -> “Weak” 
// mySecurePass123 -> “Moderate” 
// !@!pass1 -> “Moderate”
// @S3cur1ty -> “Strong”

/** from "." because js file and test file have same name */
import { evaluatePassword } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
it("should return Invalid for stonk input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("stonk");
    expect(password_safety).toBe("Invalid"); // Assertion
});
it("should return Invalid for pass word input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("pass word");
    expect(password_safety).toBe("Invalid"); // Assertion
});
it("should return Weak for password input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("password");
    expect(password_safety).toBe("Weak"); // Assertion
});
it("should return Weak for 11081992 input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("11081992");
    expect(password_safety).toBe("Weak"); // Assertion
});
it("should return Moderate for mySecurePass123 input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("mySecurePass123");
    expect(password_safety).toBe("Moderate"); // Assertion
});
it("should return Moderate for !@!pass1 input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("!@!pass1");
    expect(password_safety).toBe("Moderate"); // Assertion
});
it("should return Strong for @S3cur1ty input", () => {
    // at least 1 assertion
    const password_safety = evaluatePassword("@S3cur1ty");
    expect(password_safety).toBe("Strong"); // Assertion
});