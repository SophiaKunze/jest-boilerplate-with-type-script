export function evaluatePassword(password) {
    const INVALID = "Invalid"
    const STRONG = "Strong"
    const MODERATE = "Moderate"
    const WEAK = "Weak"
    
    let fulfilledCriteria = 0;

    // if password is too short or white spaces
    const hasShortLength = password.length < 6
    const hasWiteSpaces = password.length > password.replace(/\s/g, "").length
    if (hasShortLength || hasWiteSpaces) {
        return INVALID;
    }
    // increase points for length
    if (password.length >= 8) {
        fulfilledCriteria += 1
    }
    // increase points for lowercase
    if (/[a-z]/.test(password)) {
        fulfilledCriteria += 1
    };
    // increase points for uppercase
    if (/[A-Z]/.test(password)) {
        fulfilledCriteria += 1
    };
    // increase points for digits
    if (/[0-9]/.test(password)) {
        fulfilledCriteria += 1
    };
    // increase points for special characters try /\W/
    if (/\W/.test(password)) {
        fulfilledCriteria += 1
    };
    console.log(`${password} has ${fulfilledCriteria} fulfilledCriteria`)
    

    // if (fulfilledCriteria >= 1 && fulfilledCriteria <= 2) {
    //     return WEAK
    // };
    // if (fulfilledCriteria >= 3 && fulfilledCriteria <= 4) {
    //     return MODERATE
    // }
    // else {
    //     return STRONG
    // }

    // usage of switch instead of if statements
    switch (fulfilledCriteria) {
        case 5:
            return STRONG;
        case 4:
        case 3:
            return MODERATE;
        case 2:
        case 1:
            return WEAK
        default:
            return INVALID
    }
}