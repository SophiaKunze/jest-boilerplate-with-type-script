// anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa'] 
 
// anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer'] 
 
// anagrams('laser', ['lazing', 'lazy',  'lacer']) => []

/** from "." because js file and test file have same name */
import { searchAnagrams } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
 it("should identify ['aabb', 'bbaa'] out of ['aabb', 'abcd', 'bbaa', 'dada'] as anagram of 'abba'", () => {
    // at least 1 assertion
    const pigLatin = searchAnagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);
    expect(pigLatin).toEqual(['aabb', 'bbaa']); // Assertion
});

it("should identify ['carer', 'racer'] out of ['crazer', 'carer', 'racar', 'caers', 'racer'] as anagrams of 'racer'", () => {
    // at least 1 assertion
    const pigLatin = searchAnagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']);
    expect(pigLatin).toEqual(['carer', 'racer']); // Assertion
});

it("should identify [] out of ['lazing', 'lazy',  'lacer'] as anagrams of 'laser', ", () => {
    // at least 1 assertion
    const pigLatin = searchAnagrams('laser', ['lazing', 'lazy',  'lacer']);
    expect(pigLatin).toEqual([]); // Assertion
});