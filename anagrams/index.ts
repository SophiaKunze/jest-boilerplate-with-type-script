export const searchAnagrams = (word, putativeAnagrams) => {
  const sortWord = (wordUnsorted) => {
    return wordUnsorted
      .toLowerCase()
      .replace(/[^a-z]/g, "")
      .split("")
      .sort()
      .join("");
  };
  const wordSorted = sortWord(word)
  return putativeAnagrams.filter(putativeAnagram => sortWord(putativeAnagram) === wordSorted)
};