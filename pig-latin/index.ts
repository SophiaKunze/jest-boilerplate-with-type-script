export const pigLatinSentence = (sentence) => {
  // define vowels as regex
  const vowels = /[aeiou]/;
  // split sentence to list of words
  const words = sentence.toLowerCase().split(" ");
  // empty list to collect all pigLatins
  const pigLatins = [];
  for (const word of words) {
    // search position of first vowel
    const positionFirstVowel = word.search(vowels);
    // if first position is vowel, sentence is appended with 'way', else sentence is appended with 'ay'
    const ending = positionFirstVowel === 0 ? "way" : "ay";
    pigLatins.push(word.substr(positionFirstVowel) + word.substr(0, positionFirstVowel) + ending);
  }
  return pigLatins.join(" ");
};
