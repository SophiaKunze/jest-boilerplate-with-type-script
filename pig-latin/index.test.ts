// pigLatinSentence("this is pig latin") ➞ "isthay isway igpay atinlay" 
 
// pigLatinSentence("wall street journal") ➞ "allway eetstray ournaljay" 
 
// pigLatinSentence("raise the bridge") ➞ "aiseray ethay idgebray" 
 
// pigLatinSentence("all pigs oink") ➞ "allway igspay oinkway"

/** from "." because js file and test file have same name */
import { pigLatinSentence } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
it("should return 'isthay isway igpay atinlay' for 'this is pig latin'", () => {
    // at least 1 assertion
    const pigLatin = pigLatinSentence("this is pig latin");
    expect(pigLatin).toBe("isthay isway igpay atinlay"); // Assertion
});

it("should return 'allway eetstray ournaljay' for 'wall street journal'", () => {
    // at least 1 assertion
    const pigLatin = pigLatinSentence("wall street journal");
    expect(pigLatin).toBe("allway eetstray ournaljay"); // Assertion
});

it("should return 'aiseray ethay idgebray' for 'raise the bridge'", () => {
    // at least 1 assertion
    const pigLatin = pigLatinSentence("raise the bridge");
    expect(pigLatin).toBe("aiseray ethay idgebray"); // Assertion
});

it("should return 'allway igspay oinkway' for 'all pigs oink'", () => {
    // at least 1 assertion
    const pigLatin = pigLatinSentence("all pigs oink");
    expect(pigLatin).toBe("allway igspay oinkway"); // Assertion
});