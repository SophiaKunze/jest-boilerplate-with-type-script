export function noStrangers(longString) {
  // transform to lowercase, remove dots and trim
  longString = longString.toLowerCase().replace(/[^a-zA-Z' ]/g, "").trim();
  // transform into list
  const words = longString.split(" ");
  
  // create js object to collect words (key) and their counter (value)
  const wordCounter = {};
  const acquaintanceCounter = {};
  const friendCounter = {};
  // loop over words
  words.forEach((word) => {
    
    // check if word in friendCounter
    if (word in friendCounter) {
      return;
    }

    // check if word in acquaintanceCounter
    if (word in acquaintanceCounter) {
      // check if value is 4
      if (acquaintanceCounter[word] === 4) {
        // add to friendCounter
        friendCounter[word] = 5;
        // remove from wordCounter
        delete acquaintanceCounter[word];
        return;
      } else {
        acquaintanceCounter[word] += 1;
        return;
      }
    }

    // check if word is in wordCounter
    if (word in wordCounter) {
      // check if value is 2
      if (wordCounter[word] === 2) {
        // add to acquaintanceCounter
        acquaintanceCounter[word] = 3;
        // remove from wordCounter
        delete wordCounter[word];
        return;
      } else {
        wordCounter[word] += 1;
        return;
      }
    } else {
      wordCounter[word] = 1;
      return;
    }
  });

  const acquaintances = [];
  const friends = [];

  // put words of acquaintanceCounter into list
  for (let word in acquaintanceCounter) {
    acquaintances.push(word);
  }
  // put words of friendCounter into list
  for (let word in friendCounter) {
    friends.push(word);
  }
  // return list containing acquaintances list and friends list
  return [acquaintances, friends];
}
