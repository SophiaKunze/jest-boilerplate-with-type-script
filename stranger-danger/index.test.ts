// noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.") ➞ [["spot" , "see"], []]
// noStrangers("hello there, there is a boot there you can swim! that's water water blue blue blue blue blue water ")

/** from "." because js file and test file have same name */
import { noStrangers } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
it("should return [['spot', 'see'], []] for input 'See Spot run. See Spot jump. Spot likes jumping. See Spot fly.'", () => {
    // at least 1 assertion
    const matched = noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.");
    expect(matched).toEqual([['spot', 'see'], []])}); // Assertion

it("should return [['there', 'water'], ['blue']] for input 'hello there, there is a boot there you can swim! that's water water blue blue blue blue blue water '", () => {
    // at least 1 assertion
    const matched = noStrangers("hello there, there is a boot there you can swim! that's water water blue blue blue blue blue water ");
    expect(matched).toEqual([['there', 'water'], ['blue']])}); // Assertion