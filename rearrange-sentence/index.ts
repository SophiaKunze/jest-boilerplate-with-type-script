export function rearrangeSentence(sentence) {
  if (sentence === " ") {
    return "";
  }
  // split sentence into list of words
  const words = sentence.split(" ");
  // empty list to collect words in correct order
  const words_sorted = [];
  // loop over length of word and check for index in word
  for (let index = 0; index <= words.length; index++) {
    words.forEach((word) => {
      // push word to to list of sorted words if index is matched
      if (word.match(index)) {
        words_sorted.push(word.replace(/[0-9]/g, ""));
      }
    });
  }
  // join sorted words into string
  const sentence_sorted = words_sorted.join(" ");
  return sentence_sorted;
}
