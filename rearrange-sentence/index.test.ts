// rearrange("is2 Thi1s T4est 3a") ➞ "This is a Test" 
 
// rearrange("4of Fo1r pe6ople g3ood th5e the2") ➞ "For the good of the 
// people" 
 
// rearrange("5weird i2s JavaScri1pt dam4n so3") ➞ "JavaScript is so damn weird" 
 
// rearrange(" ") ➞ ""


/** from "." because js file and test file have same name */
import { rearrangeSentence } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
it("should return 'This is a Test' for 'is2 Thi1s T4est 3a' input", () => {
    // at least 1 assertion
    const password_safety = rearrangeSentence("is2 Thi1s T4est 3a");
    expect(password_safety).toBe("This is a Test"); // Assertion
});

it("should return 'For the good of the people' for '4of Fo1r pe6ople g3ood th5e the2' input", () => {
    // at least 1 assertion
    const password_safety = rearrangeSentence("4of Fo1r pe6ople g3ood th5e the2");
    expect(password_safety).toBe("For the good of the people"); // Assertion
});

it("should return 'JavaScript is so damn weird' for '5weird i2s JavaScri1pt dam4n so3' input", () => {
    // at least 1 assertion
    const password_safety = rearrangeSentence("5weird i2s JavaScri1pt dam4n so3");
    expect(password_safety).toBe("JavaScript is so damn weird"); // Assertion
});

it("should return '' for ' ' input", () => {
    // at least 1 assertion
    const password_safety = rearrangeSentence(" ");
    expect(password_safety).toBe(""); // Assertion
});