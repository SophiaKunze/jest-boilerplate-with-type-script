/**
charCount("a", "edabit") ➞ 1 
 
charCount("c", "Chamber of secrets") ➞ 1 
 
charCount("B", "boxes are fun") ➞ 0 
 
charCount("b", "big fat bubble") ➞ 4 
 
charCount("e", "javascript is good") ➞ 0 
 
charCount("!", "!easy!") ➞ 2
 */

/** from "." because js file and test file have same name */
import { countInstancesOfCharacterInString } from "."

it("it should count number of 'a' in 'edabit' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("a", "edabit");
    expect(count).toBe(1); // Assertion
});

it("it should count number of 'c' in 'Chamber of secrets' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("c", "Chamber of secrets");
    expect(count).toBe(1); // Assertion
});

it("it should count number of 'B' in 'boxes are fun' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("B", "boxes are fun");
    expect(count).toBe(0); // Assertion
});

it("it should count number of 'b' in 'big fat bubble' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("b", "big fat bubble");
    expect(count).toBe(4); // Assertion
});

it("it should count number of 'e' in 'javascript is good' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("e", "javascript is good");
    expect(count).toBe(0); // Assertion
});

it("it should count number of '!' in '!easy!' and return 1", () => {
    // at least 1 assertion
    const count = countInstancesOfCharacterInString("!", "!easy!");
    expect(count).toBe(2); // Assertion
});