/**
points(1, 1) ➞ 5 
 
points(7, 5) ➞ 29 
 
points(38, 8) ➞ 100 
 
points(0, 1) ➞ 3 
 
points(0, 0) ➞ 0
 */

/** from "." because js file and test file have same name */
import { points } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
it("should add 1 two pointer and 1 three pointer to equal 5", () => {
    // at least 1 assertion
    const total = points(1, 1);
    expect(total).toBe(5); // Assertion
});

it("should add 38 two pointer and 8 three pointer to equal 100", () => {
    // at least 1 assertion
    const total = points(38, 8);
    expect(total).toBe(100); // Assertion
});

it("should add 0 two pointer and 0 three pointer to equal 0", () => {
    // at least 1 assertion
    const total = points(0, 0);
    expect(total).toBe(0); // Assertion
});

it("should throw for negative two points", () => {
    expect(() => points(-1, 1)).toThrow("Negative two points");
})

it("should throw for negative three points", () => {
    expect(() => points(1, -1)).toThrow("Negative three points");
})