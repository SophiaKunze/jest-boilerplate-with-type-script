// testJackpot(["@", "@", "@", "@"]) ➞ true 
 
// testJackpot(["abc", "abc", "abc", "abc"]) ➞ true 
 
// testJackpot(["SS", "SS", "SS", "SS"]) ➞ true 
 
// testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false 
 
// testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false

/** from "." because js file and test file have same name */
import { isJackpot } from "."

it(`should compare array with four times \@ and return true`, () => {
    // at least 1 assertion
    const equal = isJackpot(["@", "@", "@", "@"]);
    expect(equal).toBe(true); // Assertion
});

it(`should compare array with four times "abc" and return true`, () => {
    // at least 1 assertion
    const equal = isJackpot(["abc", "abc", "abc", "abc"]);
    expect(equal).toBe(true); // Assertion
});

it(`should compare array with four times "SS" and return true`, () => {
    // at least 1 assertion
    const equal = isJackpot(["SS", "SS", "SS", "SS"]);
    expect(equal).toBe(true); // Assertion
});

it(`should compare all elements in ["&&", "&", "&&&", "&&&&"] and return false`, () => {
    // at least 1 assertion
    const equal = isJackpot(["&&", "&", "&&&", "&&&&"]);
    expect(equal).toBe(false); // Assertion
});

it(`should compare all elements in ["SS", "SS", "SS", "Ss"] and return false`, () => {
    // at least 1 assertion
    const equal = isJackpot(["SS", "SS", "SS", "Ss"]);
    expect(equal).toBe(false); // Assertion
});