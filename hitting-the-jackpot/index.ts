export function isJackpot(arr) {
    if (arr.length != 4) {
        throw new Error("Array should have a length of four")};
    // another option would be to use a set (check if length of set is 1)
        const allEqual = arr => arr.every(v => v === arr[0])
    return allEqual(arr) 
}