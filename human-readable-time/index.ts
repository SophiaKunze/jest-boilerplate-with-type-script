export const calculateHumanReadable = (totalSeconds) => {
    // get hours
    const remainingSecondsFromHours = totalSeconds % 3600
    const hours = `0${(totalSeconds - remainingSecondsFromHours) / 3600}`
    // get minutes
    const remainingSecondsFromMinutes = remainingSecondsFromHours % 60
    const minutes = `0${(remainingSecondsFromHours - remainingSecondsFromMinutes) / 60}`
    // get seconds
    const seconds = `0${remainingSecondsFromMinutes}`
    
    return `${hours.slice(-2)}:${minutes.slice(-2)}:${seconds.slice(-2)}`
}