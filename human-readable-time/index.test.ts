// humanReadable(0) -> '00:00:00' 
// humanReadable(59) -> '00:00:59' 
// humanReadable(60) -> '00:01:00' 
// humanReadable(90) -> '00:01:30' 
// humanReadable(3599) -> '00:59:59' 
// humanReadable(3600) -> '01:00:00' 
// humanReadable(45296) -> '12:34:56' 
// humanReadable(86399) -> '23:59:59' 
// humanReadable(86400) -> '24:00:00' 
// humanReadable(359999) -> '99:59:59'

/** from "." because js file and test file have same name */
import { calculateHumanReadable } from "."

/**
 * Arg 1: Describes the test.
 * Arg 2: Fn - Code for test
 */
 it("should return 0 seconds as '00:00:00'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(0);
    expect(humanReadable).toEqual('00:00:00'); // Assertion
});

it("should return 59 seconds as '00:00:59'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(59);
    expect(humanReadable).toEqual('00:00:59'); // Assertion
});

it("should return 60 seconds as '00:01:00'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(60);
    expect(humanReadable).toEqual('00:01:00'); // Assertion
});

it("should return 90 seconds as '00:01:30'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(90);
    expect(humanReadable).toEqual('00:01:30'); // Assertion
});

it("should return 3599 seconds as '00:59:59'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(3599);
    expect(humanReadable).toEqual('00:59:59'); // Assertion
});

it("should return 3600 seconds as '01:00:00'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(3600);
    expect(humanReadable).toEqual('01:00:00'); // Assertion
});

it("should return 45296 seconds as '12:34:56'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(45296);
    expect(humanReadable).toEqual('12:34:56'); // Assertion
});

it("should return 86399 seconds as '23:59:59'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(86399);
    expect(humanReadable).toEqual('23:59:59'); // Assertion
});

it("should return 86400 seconds as '24:00:00'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(86400);
    expect(humanReadable).toEqual('24:00:00'); // Assertion
});

it("should return 359999 seconds as '99:59:59'", () => {
    // at least 1 assertion
    const humanReadable = calculateHumanReadable(359999);
    expect(humanReadable).toEqual('99:59:59'); // Assertion
});